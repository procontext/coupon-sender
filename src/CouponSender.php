<?php

namespace Procontext\CouponSender;

use Procontext\CouponSender\Exceptions\ClientErrorException;
use Procontext\CouponSender\Exceptions\CouponDisabledException;
use Procontext\CouponSender\Exceptions\FileNotFoundException;
use Procontext\CouponSender\Exceptions\InfoErrorException;
use Procontext\Mailer\SyncSender;
use Procontext\Mailer\Mailer;

class CouponSender
{
    private SyncSender $sender;
    private Mailer $mailer;

    public function __construct(
        SyncSender $sender,
        Mailer $mailer
    ) {
        $this->sender = $sender;
        $this->mailer = $mailer;
    }

    public function send(Coupon $coupon, string $email, string $mailBody): bool
    {
        if (!env('COUPON_ENABLE', false)) {
            throw new CouponDisabledException();
        }

        if (!$this->sendCoupon($coupon, $email)) {
            throw new ClientErrorException();
        }

        if (env('COUPON_COUNTER', false)) {
            $this->count($coupon);
        }

        if (!$this->sendInfo($mailBody)) {
            throw new InfoErrorException();
        }

        return true;
    }

    private function sendCoupon(Coupon $coupon, string $email): bool
    {
        if (!file_exists($coupon->getFilePath())) {
            throw new FileNotFoundException();
        }

        $file = file_get_contents($coupon->getFilePath());
        $body = '<img style="max-width: 100%" src="data:image/png;base64,' . base64_encode($file) . '" />';

        return $this->sender->send(
            $coupon->getTitle(),
            $coupon->getSubject(),
            $body,
            [$email => '']
        );
    }

    private function sendInfo(string $mailBody): bool
    {
        return $this->mailer->send($mailBody, 'Отправлен купон');
    }

    private function count(Coupon $coupon): int
    {
        $count = (int) file_get_contents($coupon->getCounterPath());
        $count++;
        file_put_contents($coupon->getCounterPath(), $count);

        return $count;
    }
}
