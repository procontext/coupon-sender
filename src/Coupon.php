<?php

namespace Procontext\CouponSender;

abstract class Coupon
{
    protected string $title;
    protected string $subject;

    public function __construct()
    {
        $this->setTitle();
        $this->setSubject();
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    abstract public function setTitle(string $title = null): void;
    abstract public function setSubject(string $subject = null): void;

    abstract public function getFilePath(): string;
    abstract public function getCounterPath(): string;
}
