<?php

namespace Procontext\CouponSender\Exceptions;

use Throwable;

class FileNotFoundException extends CouponException
{
    public function __construct(string $message = 'Файл по указанному пути не найден.', int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
