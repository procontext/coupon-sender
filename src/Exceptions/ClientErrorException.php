<?php

namespace Procontext\CouponSender\Exceptions;

use Throwable;

class ClientErrorException extends CouponException
{
    public function __construct(string $message = 'Ошибка при отправке купона клиенту.', int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
