<?php

namespace Procontext\CouponSender\Exceptions;

use Throwable;

class InfoErrorException extends CouponException
{
    public function __construct(string $message = 'Ошибка при отправке отбивки.', int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
