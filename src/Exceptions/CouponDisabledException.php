<?php

namespace Procontext\CouponSender\Exceptions;

use Throwable;

class CouponDisabledException extends CouponException
{
    public function __construct(string $message = 'Отправка купонов приостановлена.', int $code = 0, ?Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
